<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Picture;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        // Equivalent : SELECT * FROM pictures
        // findAll() retourne tous les résultats trouvés dans la table "pictures"
        $resultPictures = $this->getDoctrine()->getRepository(Picture::class)->findAll();

        // Création d'une page par page
        // paginate() attend 3 arguments :
        // 1. La collection contenant tous les résultats(pour nous, tous les images)
        // 2. La page sur laquelle nous sommes(cdr : page 1, page 2,...)
        // 3. Le nombre déléments par page
        $page = $request->query->getInt('page', 1);
        $picture = $paginator->paginate(
            $resultPictures, 
            $page === 0 ? 1 : $page,
            16
        );

        // Sélectionne toutes les catégories
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();


        return $this->render('home/index.html.twig', [
            'pictures' => $picture,
            'categories' => $categories
        ]);
    }

/**
     * @Route("/category/{id}", name="pictures_by_category")
     */
    public function picturesByCategory($id, PaginatorInterface $paginator, Request $request)
    {
        // dd veut dire "Dump Data", équivalent de var_dump()
        // Arrête tout chargement dès que le programme "tombe" dessus et affiche son contenu
        // dd($id);

        // dump(), équivalent de var_dump()
        // Laisse la page se charger complètement et affiche le contenu du "dump" dans la debug bar.
        // dump($id);

        // find() sélectionne UN SEUL enregistrement selon son ID
        $category = $this->getDoctrine()->getRepository(Categorie::class)->find($id);

        // Si la catégorie est introuvable, alors on génère une erreur 404
        if (!$category) {
            throw $this->createNotFoundException('La catégorie n\'existe pas');
        }

        // Page par page pour les images liées à la catégorue
        $page = $request->query->getInt('page', 1);
        $pictures = $paginator->paginate(
            $category->getPictures(),
            $page === 0 ? 1 : $page,
            16
        );

        return $this->render('home/picturesByCategory.html.twig', [
            'category' => $category,
            'pictures' => $pictures
        ]);
    }

/**
     * "requirements" permet de valider le type de la donnée passée en paramètre
     * @Route("/show/picture/{id}", name="show_picture", requirements={"id"="\d+"})
     */
    public function showPicture($id, PaginatorInterface $paginator, Request $request)
    {
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);

        if (!$picture) {
            throw $this->createNotFoundException('Cette image n\'existe pas');
        }

        // Page par page pour les images
        $page = $request->query->getInt('page', 1);
        $photos = $paginator->paginate(
            $picture->getCategory()->getPictures(),
            $page === 0 ? 1 : $page,
            16
        );

        return $this->render('home/showPicture.html.twig', [
            'picture' => $picture,
            'photos' => $photos
        ]);
    }
}