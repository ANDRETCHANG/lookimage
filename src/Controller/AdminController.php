<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Form\CategoryType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Symfony\Component\Routing\Annotation\Route;


class AdminController extends AbstractController
{
    /**
     * @Route("/admin/categories", name="admin")
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        $query = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        $page = $request->query->getInt('page', 1);
        $categories = $paginator->paginate(
            $query,
            $page === 0 ? 1 : $page,
            20
        );

        return $this->render('admin/index.html.twig', [
            'categories' => $categories
        ]);
    }
    
    /**
     *  @Route("/admin/categories/new", name="admin_categories_new")
     */
    public function new(Request $request)
    {
        // Instanciation de l'entité désiré
        $category = new Categorie();

        // Création du formulaire
        $formNewCategory = $this->createForm(CategoryType::class, $category);
        $formNewCategory->handleRequest($request);

        // Traitement des données si le formulaire est envoyé
        // et que les validations sont acceptées
        if ($formNewCategory->isSubmitted() && $formNewCategory->isValid()) {
            // Transform le nom de la catégorie en slug
            $category->setSlug((new AsciiSlugger())->slug(strtolower($category->getName())));

            // Insertion en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($category);
            $doctrine->flush();

            // Message flash
            $this->addFlash('success', 'La catégorie a bien été créée');

            //retour à mon tableau de catégorie
            return $this->redirectToRoute('admin');
        }

        return $this->render('admin/new.html.twig', [
            'formNewCategory' => $formNewCategory->createView()
        ]);
    }
}