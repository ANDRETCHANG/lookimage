<?php

namespace App\Controller;

use App\Entity\Picture;
use App\Form\UploadPictureType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccountController extends AbstractController
{
    /**
     * @Route("/account", name="account")
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        /** 
         * $this->getUser() est l'équivalent de "app.user" en twig
         * Par exemple, si je veux le prénom : $this->getUser()->getFirstname()
         */
        
        /* $query = $this->getDoctrine()->getRepository(Picture::class)->findBy([
            'user' => $this->getUser()
        ]); */

        $page = $request->query->getInt('page', 1);
        $pictures = $paginator->paginate(
            $this->getUser()->getPictures(),
            $page === 0 ? 1 : $page,
            20
        );

        return $this->render('account/index.html.twig', [
            'pictures' => $pictures
        ]);
    }


    /**
     * @Route("/account/new/picture", name="account_new_picture")
     */
    public function new(Request $request)
    {
        // Instancie l'entité "Picture"
        $picture = new Picture();

        // Premier paramètre : Le formulaire dont ont a besoin
        // Deuxième paramètre : l'objet de l'entité à vide
        $formUpload = $this->createForm(UploadPictureType::class, $picture, [
            'validation_groups' => 'new_picture'
        ]);
        $formUpload->handleRequest($request);
        
        // Vérifie si le formulaire est envoyé et valide !
        if ($formUpload->isSubmitted() && $formUpload->isValid()) {
            
            // Ajoute la date du jour
            $picture->setCreatedAt(new \DateTimeImmutable());

            // Passe l'utilisateur actuellement connecté à notre setter
            $picture->setUser($this->getUser());

            // Insertion en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($picture);
            $doctrine->flush();

            // Création d'un message flash
            $this->addFlash('success', 'Merci pour votre partage !');

            // Redirection vers la page d'accueil
            return $this->redirectToRoute('account');
        }

        // Si vous voulez capter toutes les erreurs d'un formulaire pour les afficher
        // où vous le souhaitez.
        /* if ($formUpload->isSubmitted() && !$formUpload->isValid()) {
            $errors = $formUpload->getErrors(true);
        } */

        return $this->render('account/new.html.twig', [
            'formUpload' => $formUpload->createView(),
            // 'errors' => $errors
        ]);
    }

  /**
     * @Route("/account/edit/picture/{id}", name="account_edit_picture")
     */
    public function edit($id, Request $request)
    {
        // Sélectionner un enregistrement en BDD
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);

        // Si l'image n'existe pas, on retourne une erreur 404
        // Ou si l'image n'appartient pas à l'utilisateur connecté, on retourne une erreur 404C
        if (!$picture || $this->getUser() !== $picture->getUser()) {
            throw $this->createNotFoundException('Cette image n\'existe pas'); // Erreur 404 - Ressource non trouvée
            // throw $this->createAccessDeniedException('Cette image n\'existe pas'); // Erreur 403 - Accès refusé
        }

        $formEdit = $this->createForm(UploadPictureType::class, $picture, [
            'validation_groups' => 'edit_picture'
        ]);
        $formEdit->handleRequest($request);

        if ($formEdit->isSubmitted() && $formEdit->isValid()) {
            $picture->setUpdatedAt(new \DateTimeImmutable());

            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($picture);
            $doctrine->flush();

            $this->addFlash('success', 'Les modifications ont bien été enregistrées');

            return $this->redirectToRoute('account');
        }

        return $this->render('account/edit.html.twig', [
            'formEdit' => $formEdit->createView()
        ]);
    }

    /**
     * @Route("/account/delete/picture/{id}", name="account-delete_picture")
     */
    public function delete($id)
    {
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);

        // Si l'image n'existe pas, on retourne une erreur 404
        // Ou si l'image n'appartient pas à l'utilisateur connecté, on retourne une erreur 404C
        if (!$picture || $this->getUser() !== $picture->getUser()) {
            throw $this->createNotFoundException('Cette image n\'existe pas'); // Erreur 404 - Ressource non trouvée
            // throw $this->createAccessDeniedException('Cette image n\'existe pas'); // Erreur 403 - Accès refusé
        }
        
        $doctrine = $this->getdoctrine()->getManager();
        $doctrine -> remove($picture);
        $doctrine->flush();

        $this->addFlash('success', 'La photo a bien été supprimé');

        return $this->redirectToRoute('account');
    }
}
