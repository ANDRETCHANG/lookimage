<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'required' => true, // Rends le champs obligatoire en ajoutant l'attribut "required"
                'label' => 'Prénom', // Choix du nom du label, si vous en voulez pas, mettez la valeur à "false"
                'attr' => [
                    'placeholder' => 'Votre prénom',
                    // 'class' => 'nameClassCss deuxiemeClassCss', // Config pour ajouter des classes CSS au champs input
                ],
                'constraints' => [ // Contraintes de validations. Vérification des données côté serveur.
                    new NotBlank([
                        'message' => 'Veuillez saisir un prénom'
                    ])
                ]
            ])
            ->add('lastname', TextType::class, [
                'required' => true, // Rends le champs obligatoire en ajoutant l'attribut "required"
                'label' => 'Nom', // Choix du nom du label, si vous en voulez pas, mettez la valeur à "false"
                'constraints' => [ // Contraintes de validations. Vérification des données côté serveur.
                    new NotBlank([
                        'message' => 'Veuillez saisir un nom'
                    ])
                ]
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'Adresse e-mail',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez saisir une adresse e-mail'
                    ]),
                    new Email([
                        'message' => 'Votre adresse e-mail est invalide'
                    ])
                ]
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false, // Spécifie que ce champ n'est pas relié à l'entité en cours
                'label' => 'En soumettant ce formulaire, j’accepte que LookImage conserve mes données personnelles via ce formulaire. Toutes mes données seront vendues à des entreprises afin de me proposer des isolations à un euros.',
                'constraints' => [
                    new IsTrue([
                        'message' => 'Vous devez accepter les RGPD',
                    ]),
                ],
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Les mots de passe ne correspondent pas',
                'first_options' => ['label' => 'Mot de passe'],
                'second_options' => ['label' => 'Confirmer le mot de passe'],
                'required' => true,
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez entrer un mot de passe',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Votre mot de passe doit contenir {{ limit }} caractères minimum',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
