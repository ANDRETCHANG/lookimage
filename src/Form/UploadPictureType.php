<?php

namespace App\Form;

use App\Entity\Categorie;
use App\Entity\Picture;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;
use Vich\UploaderBundle\Form\Type\VichImageType;

class UploadPictureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('imageFile', VichImageType::class, [
                'required' => false,
                'label' => 'Sélectionner une photo',
                // Option de VichImageType
                'allow_delete' => false,
                'download_uri' => false,
                'imagine_pattern' =>'thumbnail',
                // ---
                
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez choisir une photo',
                        'groups' => ['new_picture']
                    ]),
                    new Image([
                        'maxSize' => '2M',
                        'maxSizeMessage' => 'Le fichier ne doit pas dépasser les 2Mo',
                        'mimeTypes' => [
                            'image/gif', 'image/png', 'image/jpeg', 'image/webp'
                        ],
                        'mimeTypesMessage' => 'Cette image est invalide',
                        'groups' => ['new_picture', 'edit_picture']
                    ])
                ]
            ])
            ->add('description', TextareaType::class, [
                'required' => true,
                'label' => 'Description de la photo',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez saisir une description',
                        'groups' => ['new_picture', 'edit_picture']
                    ])
                ]
            ])
            ->add('tags', TextType::class, [
                'required' => true,
                'label' => 'Tags',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez saisir un ou plusieurs tags',
                        'groups' => ['new_picture', 'etit_picture']
                    ])
                ]
            ])
            // Génère un menu déroulant contenant les données de la table "categorie"
            ->add('category', EntityType::class, [
                'required' => true,
                'label' => 'Catégorie de la photo',
                'class' => Categorie::class,
                'choice_label' => 'name'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Picture::class,
        ]);
    }
}
